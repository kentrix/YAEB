package gq.meme.kentrix.yaeb.layouts

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.varunest.sparkbutton.SparkButton
import com.varunest.sparkbutton.SparkEventListener
import es.dmoral.toasty.Toasty
import gq.meme.kentrix.yaeb.R
import gq.meme.kentrix.yaeb.dao.DAO
import gq.meme.kentrix.yaeb.dto.FavResponseDeseialiser
import gq.meme.kentrix.yaeb.dto.ListByTagsResponse
import gq.meme.kentrix.yaeb.utils.*
import gq.meme.kentrix.yaeb.viewmodels.*
import io.reactivex.observers.DisposableMaybeObserver
import kotlinx.coroutines.experimental.android.UI
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textColorResource
import java.io.File
import java.util.*

class ThumbnailViewAdapter(private val parentFragment: Fragment) : RecyclerView.Adapter<ThumbnailViewAdapter.ThumbnailViewItemHolder>() {

    private val TN_ITEM = 1
    private val LOADING_PROGRESS = 0
    private val listByTagsResponse: List<ListByTagsResponse>
        get() = ObservableStateContainer.listByTagsResponse
    private var observerHolder: Observer? = null
    private var isLoadFailed: Boolean = false

    fun notifyLoadFailed() {
        isLoadFailed = true
        notifyItemChanged(itemCount - 1)
    }

    override fun getItemViewType(position: Int): Int {
        val last = listByTagsResponse.getOrNull(position)
        return if (last == null) LOADING_PROGRESS else TN_ITEM
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        observerHolder = Observer { _, arg ->
            when (arg) {
                is RefreshView -> {
                    Log.d(CLASS_NAME, "Received RefreshView, ${arg.posStart} ${arg.siz}")
                    notifyItemRangeChanged(arg.posStart, arg.siz)
                    notifyDataSetChanged()
                }
                is NoMoreData -> return@Observer
                is LoadFailed -> Log.d(CLASS_NAME, "Load failed: ex ${arg.ex}")
                else -> Log.wtf(CLASS_NAME, "Unknown message received")
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: ThumbnailViewItemHolder) {
        super.onViewDetachedFromWindow(holder)
        if (observerHolder != null) {
            ObservableStateContainer.deleteObserver(observerHolder)
            observerHolder = null
        }
    }

    override fun getItemCount(): Int = listByTagsResponse.size + 1

    override fun onBindViewHolder(holder: ThumbnailViewItemHolder, position: Int) {
        when (holder) {
            is ThumbnailViewHolder -> {
                Log.d(CLASS_NAME, "Binding viewholder for id ${listByTagsResponse[position].id}")

                val sparkBtnList = listOf(holder.upvoteBtn, holder.downvoteBtn, holder.favBtn)
                val loggedOnUser = DAO.getLoggedOnUser()
                val item = listByTagsResponse[holder.adapterPosition]

                holder.imageView.setImageURI(item.sample_url)
                holder.favText.text = item.fav_count.toString()
                holder.upvoteText.text = item.score.toString()
                // Set text colour for upvote text, positive score shall have a red text
                // whereby negative score shall have a green text
                if (item.score >= 0) {
                    holder.upvoteText.textColorResource = R.color.fav_red
                } else {
                    holder.upvoteText.textColorResource = R.color.upvote_green
                }

                if (loggedOnUser == null) {
                    sparkBtnList.forEach {
                        it.isClickable = false
                        it.setOnTouchListener { v, _ ->
                            showRequireUserLoginToast(v.context)
                            true
                        }
                    }
                } else {
                    holder.favBtn.setOnClickListener {
                        ObservableStateContainer.requestFav(item.id.toString(), { id, favResponse ->
                            DAO.addFavForUser(loggedOnUser, id)
                            holder.favBtn.playAnimation()
                            holder.favBtn.isChecked = true
                            holder.favText.text = (item.fav_count + 1).toString()
                            Unit
                        }, { id, any ->
                            when (any) {
                                is FavAlreadyExists -> {
                                    holder.favBtn.playAnimation()
                                    holder.favBtn.isChecked = true
                                }
                                else -> {
                                    holder.favBtn.isChecked = false
                                    showActionFailedToast(it.context)
                                }
                            }
                            Unit
                        })
                    }
                }

                val isPostFaved = DAO.getIsPostFaved(item.id.toString())

                if (isPostFaved != null && isPostFaved) {
                    holder.favBtn.isChecked = true
                }

                holder.favBtn.setEventListener(object : SimpleSparkEventListener() {
                    override fun onEvent(button: ImageView?, buttonState: Boolean) {
                        if (!DAO.isUserLoggedIn()) {
                            return
                        }
                        when (buttonState) {
                            true -> {
                            }
                            false -> {
                            }
                        }
                    }

                })

                val listener = View.OnClickListener {
                    (parentFragment as? OnImageClickedListener)?.onImageClicked(holder.adapterPosition)
                }

                holder.imageView.setOnClickListener(listener)
                holder.listByTagsResponse = item
            }
            is ThumbnailProgressViewHolder -> {
                if (isLoadFailed) {
                    holder.reloadButton.visibility = View.VISIBLE
                    holder.wholeProgress.visibility = View.GONE
                    if (!holder.reloadButton.hasOnClickListeners()) {
                        holder.reloadButton.setOnClickListener {
                            ObservableStateContainer.loadMore()
                            holder.reloadButton.visibility = View.GONE
                            holder.wholeProgress.visibility = View.VISIBLE
                        }
                    }
                } else {
                    holder.wholeProgress.isIndeterminate = true
                    holder.reloadButton.visibility = View.GONE
                }
            }
            else -> Log.wtf(CLASS_NAME, "Unknown holder received!")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThumbnailViewItemHolder {
        return if (viewType == TN_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.thumbnail_view, parent, false)
            ThumbnailViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.tn_progress_bar, parent, false)
            ThumbnailProgressViewHolder(view)
        }
    }

    abstract class ThumbnailViewItemHolder(view: View) : RecyclerView.ViewHolder(view)

    class ThumbnailViewHolder(view: View) : ThumbnailViewItemHolder(view) {
        val favBtn: SparkButton by bindView(R.id.thumbnailFavButton)
        val favBtnWrapper: Button by bindView(R.id.thumbnailFavButtonWrapper)
        val upvoteBtn: SparkButton by bindView(R.id.thumbnailUpvoteButton)
        val upvoteBtnWrapper: Button by bindView(R.id.thumbnailUpvoteButtonWrapper)
        val downvoteBtn: SparkButton by bindView(R.id.thumbnailDownvoteButton)
        val imageView: SimpleDraweeView by bindView(R.id.thumbnailImage)
        val favText: TextView by bindView(R.id.thumbnailFavButtonText)
        val upvoteText: TextView by bindView(R.id.thumbnailUpvoteButtonText)
        var subscriber: DisposableMaybeObserver<File>? = null
        var listByTagsResponse: ListByTagsResponse? = null
    }

    class ThumbnailProgressViewHolder(view: View) : ThumbnailViewItemHolder(view) {
        val wholeProgress: ProgressBar by bindView(R.id.TNProgressBar)
        val reloadButton: Button by bindView(R.id.reloadButton)
    }

    interface OnImageClickedListener {
        fun onImageClicked(pos: Int)
    }


}
