package gq.meme.kentrix.yaeb.dto

data class UserProfile(
        val userName: String, // Global unique
        val apiKey: String,
        val favIds: MutableSet<String>,
        val downvotedIds: MutableSet<String>,
        val upvotedIds: MutableSet<String>,
        val blackList: MutableSet<String>
)
