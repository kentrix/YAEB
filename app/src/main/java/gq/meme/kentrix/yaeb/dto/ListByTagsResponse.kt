package gq.meme.kentrix.yaeb.dto

import android.os.Parcel
import android.os.Parcelable
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

data class ListByTagsResponse(
        val id: Int,
        val tags: String,
        val score: Int,
        val fav_count: Int,
        val preview_url: String,
        val sample_url: String,
        val file_url: String,
        val file_ext: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    object Deserializer : ResponseDeserializable<List<ListByTagsResponse>> {
        override fun deserialize(content: String): List<ListByTagsResponse>? {
            val token = object : TypeToken<List<ListByTagsResponse>>() {
            }.type
            return Gson().fromJson(content, token)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(tags)
        parcel.writeInt(score)
        parcel.writeInt(fav_count)
        parcel.writeString(preview_url)
        parcel.writeString(sample_url)
        parcel.writeString(file_url)
        parcel.writeString(file_ext)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ListByTagsResponse> {
        override fun createFromParcel(parcel: Parcel): ListByTagsResponse {
            return ListByTagsResponse(parcel)
        }

        override fun newArray(size: Int): Array<ListByTagsResponse?> {
            return arrayOfNulls(size)
        }
    }
}

