package gq.meme.kentrix.yaeb.utils

import android.support.v7.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayoutManager

abstract class InfiniteScrollListener(
        private var prevTotalCount: Int = 0,
        private var loading: Boolean = true,
        private val layoutManager: FlexboxLayoutManager
) : RecyclerView.OnScrollListener() {


    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visCount = recyclerView?.childCount ?: 0
        val lastPos = layoutManager.findLastVisibleItemPosition()
        val totalCount = layoutManager.itemCount
        val firstPos = layoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalCount > prevTotalCount) {
                loading = false
                prevTotalCount = totalCount
            }
        }

        if (!loading && totalCount <= lastPos + 10) {
            loading = true
            onLoadMore()
        }
    }

    abstract fun onLoadMore()
}