package gq.meme.kentrix.yaeb.application

import android.app.Application
import android.content.Context
import android.os.Looper
import com.facebook.drawee.backends.pipeline.Fresco
import io.paperdb.Paper
import java.io.File

class YEAB : Application() {

    override fun onCreate() {
        super.onCreate()
        Paper.init(this)
        instance = this
        Fresco.initialize(this)
        mkdirP(pathRelativeToInternalCache(TN_DIR))
        mkdirP(pathRelativeToInternalCache(FULL_IMAGE_DIR))
    }

    companion object {
        private lateinit var instance: YEAB
        const val TN_DIR = "Samples"
        const val FULL_IMAGE_DIR = "Full"

        private val context: Context
                get() = instance.applicationContext

        fun mkdirP(file: File) {
            if (file.exists()) {
                if ((!file.isDirectory || !file.canWrite() || !file.canRead())) {
                    throw Exception("Fatal: Unable to resolve a directory for read and write for $file")
                }
            } else {
                file.mkdir()
            }
        }

        fun pathRelativeToInternalCache(fileName: String): File = File(context.filesDir, fileName)
        fun pathRelativeToInternalCacheTN(fileName: String): File = File(pathRelativeToInternalCache(TN_DIR), fileName)
        fun pathRelativeToInternalCacheFull(fileName: String): File = File(pathRelativeToInternalCache(FULL_IMAGE_DIR), fileName)

    }
}

