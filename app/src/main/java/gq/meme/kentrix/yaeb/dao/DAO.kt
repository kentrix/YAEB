package gq.meme.kentrix.yaeb.dao

import android.util.Log
import gq.meme.kentrix.yaeb.dto.ImageObject
import gq.meme.kentrix.yaeb.dto.UserProfile
import gq.meme.kentrix.yaeb.utils.CLASS_NAME
import io.paperdb.Paper

object DAO {

    private fun formatUserDataSave(userData: UserProfile): String =
            "UserProfile.${userData.userName}"

    private fun formatUserDataSave(userName: String): String =
            "UserProfile.$userName"

    private fun <T> doOpsOnUser(userName: String, cb: (UserProfile) -> T) : T? {
        val userProfile = getUserData(userName)
        return if (userProfile != null) {
            cb(userProfile)
        } else {
            null
        }
    }

    private const val LOGGED_ON_USER = "LOGGED_ON_USER"
    fun getLoggedOnUser() : UserProfile? =
        Paper.book().read<UserProfile>(LOGGED_ON_USER)

    fun setLoggedOnUser(userProfile: UserProfile) =
            Paper.book().write<UserProfile>(LOGGED_ON_USER, userProfile)

    fun logoutUser() =
            Paper.book().delete(LOGGED_ON_USER)

    /**
     * Add {id} favorite to the user
     * @return Set<String>, the set of new favorited id, null if not successful
     */
    fun addFavForUser(userName: String, id: String) : Set<String>? {
        return doOpsOnUser(userName) {
            it.favIds.add(id)
            saveUserData(it)
            it.favIds
        }
    }

    fun addFavForUser(userProfile: UserProfile, id: String) : Set<String>? {
        userProfile.favIds.add(id)
        saveUserData(userProfile)
        return userProfile.favIds
    }


    /**
     * Delete {id} favorite to the user
     * @return Set<String>, the set of new favorited id, null if not successful
     */
    fun delFavForUser(userName: String, id: String) : Set<String>? {
        return doOpsOnUser(userName) {
            it.favIds.remove(id)
            saveUserData(it)
            it.favIds
        }
    }

    /**
     * Upvote post {id} for user
     * @return Set<String>, the set of upvoted posts, null if not successful
     */
    fun upvoteForUser(userName: String, id: String) : Set<String>? {
        return doOpsOnUser(userName) {
            it.upvotedIds.add(id)
            it.downvotedIds.remove(id)
            it.upvotedIds
        }
    }

    /**
     * Downvote post {id} for user
     * @return Set<String>, the set of downvoted posts, null if not successful
     */
    fun downvoteForUser(userName: String, id: String) : Set<String>? {
        return doOpsOnUser(userName) {
            it.downvotedIds.add(id)
            it.upvotedIds.remove(id)
            it.downvotedIds
        }
    }

    /**
     * Get is post faved
     * @see DAO.getLoggedOnUser
     * @return null if user is not logged in, true if post is favorited, false otherwise
     */
    fun getIsPostFaved(postID: String) : Boolean? {
        val loggedOnUser = getLoggedOnUser()
        return loggedOnUser?.favIds?.contains(postID)
    }

    fun getPostVoteStatus(postID: String) : VoteStatus? {
        val loggedOnUser = getLoggedOnUser()
        return when {
            loggedOnUser == null -> null
            loggedOnUser.upvotedIds.contains(postID) -> VoteStatus.Upvoted
            loggedOnUser.downvotedIds.contains(postID) -> VoteStatus.Downvoted
            else -> VoteStatus.None
        }
    }

    fun isUserLoggedIn() : Boolean = getLoggedOnUser() != null

    fun saveUserData(userData: UserProfile) {
        Paper.book().write(formatUserDataSave(userData), userData)
    }

    fun getUserData(userName: String): UserProfile? =
            Paper.book().read<UserProfile>(formatUserDataSave(userName))

    fun getImageObjectForId(id: Int): ImageObject? =
        Paper.book().read<ImageObject>(id.toString())

    fun getTNFileForId(id: Int): String? =
            Paper.book().read<ImageObject>(id.toString())?.relativePathTN

    fun getFullPathForId(id: Int): String? =
            Paper.book().read<ImageObject>(id.toString())?.relativePathFull

    fun saveImageObj(imageObject: ImageObject) =
            Paper.book().write(imageObject.id.toString(), imageObject)

    fun updateTNFileForId(id: Int, path: String?) {
        Log.d(CLASS_NAME, "Update for id $id for TN path @ $path")
        val result = Paper.book().read<ImageObject>(id.toString())
        val newObj: ImageObject = if (result == null) {
            ImageObject(id, path, null, emptyList(), emptyList(), emptyList())
        } else {
            ImageObject(id, relativePathTN = path, relativePathFull = result.relativePathFull,
                    favedByUser = result.favedByUser, downvotedByUser= result.downvotedByUser,
                    upvotedByUser = result.upvotedByUser)
        }
        Paper.book().write(id.toString(), newObj)
    }

    fun updateFullPathFileForId(id: Int, path: String?) {
        Log.d(CLASS_NAME, "Update for id $id for full path @ $path")
        val result = Paper.book().read<ImageObject>(id.toString())
        val newObj: ImageObject = if (result == null) {
            ImageObject(id, path, null, emptyList(), emptyList(), emptyList())
        } else {
            ImageObject(id, relativePathTN = result.relativePathTN, relativePathFull = path,
                    favedByUser = result.favedByUser,  downvotedByUser= result.downvotedByUser,
                    upvotedByUser = result.upvotedByUser)
        }
        Paper.book().write(id.toString(), newObj)
    }
}

enum class VoteStatus {
    Upvoted, Downvoted, None
}