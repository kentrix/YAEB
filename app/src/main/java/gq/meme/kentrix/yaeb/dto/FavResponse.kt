package gq.meme.kentrix.yaeb.dto

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class FavResponse(
        val success: Boolean,
        val reason: String?,
        val favorited_users: String?
)
object FavResponseDeseialiser: ResponseDeserializable<FavResponse> {
    override fun deserialize(content: String): FavResponse? {
        return Gson().fromJson(content, FavResponse::class.java)
    }
}
