package gq.meme.kentrix.yaeb.utils

import android.util.ArraySet
import android.util.Log
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.result.Result
import gq.meme.kentrix.yaeb.application.YEAB
import gq.meme.kentrix.yaeb.dao.DAO
import gq.meme.kentrix.yaeb.dto.ListByTagsResponse
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import org.jetbrains.anko.doAsync
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import java.io.File
import java.util.concurrent.Future

enum class DataOrigin {
    CACHE, NETWORK
}

abstract class ImageSource {
    protected val flowableMap: MutableMap<Int, Pair<Maybe<File>, CompositeDisposable>>
    protected val requestMap: MutableMap<Int, Request>
    init {
        flowableMap = HashMap()
        requestMap = HashMap()
    }

    abstract fun getFromRemote(listByTagsResponse: ListByTagsResponse): Maybe<File>
    abstract fun getFromLocal(listByTagsResponse: ListByTagsResponse): Maybe<File>
    fun subscribe(listByTagsResponse: ListByTagsResponse, subscriber: DisposableMaybeObserver<File>) {
        Log.d(CLASS_NAME, "New sub for ${listByTagsResponse.id}")
        var obs = flowableMap[listByTagsResponse.id]
        if (obs == null) {
            obs = createObservable(listByTagsResponse) to CompositeDisposable()
            flowableMap[listByTagsResponse.id] = obs
        }
        obs.second.add(obs.first.subscribeWith(subscriber))
    }
    fun unsubscribe(listByTagsResponse: ListByTagsResponse, subscriber: DisposableMaybeObserver<File>) {
        val obs = flowableMap[listByTagsResponse.id] ?: return
        obs.second.delete(subscriber)
        Log.d(CLASS_NAME, "Unsubbing for ${listByTagsResponse.id}, remaining size ${obs.second.size()}")
        if (obs.second.size() == 0) {
            flowableMap.remove(listByTagsResponse.id)
            val req = requestMap[listByTagsResponse.id]
            if (req != null) {
                Log.d(CLASS_NAME, "Disposing request, $req")
                requestMap.remove(listByTagsResponse.id)
            }
        }
    }
    fun orderRetry(listByTagsResponse: ListByTagsResponse) {
        val obs = flowableMap[listByTagsResponse.id] ?: return
        //flowableMap[listByTagsResponse.id] = Triple(obs.first.switchIfEmpty(createObservable(listByTagsResponse)), obs.second, obs.third)
        //flowableMap[listByTagsResponse.id] = Triple(obs.first.repeat(2).firstElement(), obs.second, obs.third)
        //flowableMap[listByTagsResponse.id] = obs.first.flatMap {
        //    createObservable(listByTagsResponse)
        //} to obs.second
        //flowableMap[listByTagsResponse.id] = obs.first.retry(1) to obs.second
        //flowableMap[listByTagsResponse.id] = Triple(createObservable(listByTagsResponse), obs.second, obs.third)

    }
    open fun createObservable(listByTagsResponse: ListByTagsResponse): Maybe<File> {
        return Maybe.concat(getFromLocal(listByTagsResponse), getFromRemote(listByTagsResponse).retry { ex ->
            val retry = flowableMap.containsKey(listByTagsResponse.id)
            Log.d(CLASS_NAME, "Retrying $retry with previous ex: $ex")
            retry
        }
        )
                .firstElement()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnDispose({
                    Log.d(CLASS_NAME, "Disposing request, ${requestMap[listByTagsResponse.id]}")
                    requestMap[listByTagsResponse.id]?.cancel()
                }
                )
                .cache()
    }
}

object FullImageSource : ImageSource() {

    override fun getFromRemote(listByTagsResponse: ListByTagsResponse): Maybe<File> {
        val fileName = listByTagsResponse.file_url.split("/").last()

        return Maybe.create<File> { emitter ->
            val req = E621API.makeGetImageFromURL(listByTagsResponse.file_url).destination { response, url ->
                YEAB.pathRelativeToInternalCacheFull(fileName)
            }
            requestMap[listByTagsResponse.id] = req
            val (_, _, result) = req.response()
            when (result) {
                is Result.Success<*, *> -> {
                    DAO.updateFullPathFileForId(listByTagsResponse.id, fileName) // TODO: Perhaps move this to another observer
                    Log.d(CLASS_NAME, "Emit success @ $fileName")
                    emitter.onSuccess(YEAB.pathRelativeToInternalCacheFull(fileName))
                    return@create
                }
            }
        }
    }

    override fun getFromLocal(listByTagsResponse: ListByTagsResponse): Maybe<File> {
        return Maybe.create { emitter ->
            val databaseTNName = DAO.getFullPathForId(listByTagsResponse.id)
            if (databaseTNName != null) {
                // See if the file actually exist on filesystem and readable
                val file = YEAB.pathRelativeToInternalCacheFull(databaseTNName)
                if(file.exists() && file.canRead()) {
                    Log.d(CLASS_NAME, "Existing file in local file system for ${listByTagsResponse.id} @ ${file.absolutePath}")
                    emitter.onSuccess(file)
                } else {
                    // File not found
                    DAO.updateFullPathFileForId(listByTagsResponse.id, null)
                    emitter.onComplete()
                }
            } else {
                emitter.onComplete()
            }
        }
    }
}


object TNImageSource : ImageSource() {
    override fun getFromRemote(listByTagsResponse: ListByTagsResponse): Maybe<File> {
        val fileName = listByTagsResponse.preview_url.split("/").last()

        return Maybe.create<File> { emitter ->
            val req = E621API.makeGetImageFromURL(listByTagsResponse.preview_url).destination { response, url ->
                YEAB.pathRelativeToInternalCacheTN(fileName)
            }
            requestMap[listByTagsResponse.id] = req
            val (_, _, result) = req.response()
            when (result) {
                is Result.Success<*,*> -> {
                    DAO.updateTNFileForId(listByTagsResponse.id, fileName) // TODO: Perhaps move this to another observer
                    Log.d(CLASS_NAME, "Emit success @ $fileName")
                    emitter.onSuccess(YEAB.pathRelativeToInternalCacheTN(fileName))
                    return@create
                }
            }
        }
    }

    override fun getFromLocal(listByTagsResponse: ListByTagsResponse): Maybe<File> {
        return Maybe.create { emitter ->
            val databaseTNName = DAO.getTNFileForId(listByTagsResponse.id)
            if (databaseTNName != null) {
                // See if the file actually exist on filesystem and readable
                val file = YEAB.pathRelativeToInternalCacheTN(databaseTNName)
                if(file.exists() && file.canRead()) {
                    Log.d(CLASS_NAME, "Existing file in local file system for ${listByTagsResponse.id} @ ${file.absolutePath}")
                    emitter.onSuccess(file)
                } else {
                    // File not found
                    DAO.updateTNFileForId(listByTagsResponse.id, null)
                    emitter.onComplete()
                }
            } else {
                emitter.onComplete()
            }
        }
    }

}

@Deprecated("")
abstract class ImageProviderSource<T> {
    protected val observableMap: MutableMap<Int, Observable<T>>

    init {
        observableMap = HashMap()
    }

    fun subscribe() {
    }

    abstract fun addCacheObservable(observable: Observable<T>)
}

@Deprecated("")
abstract class ImageProvider {
    protected val futureMap: MutableMap<Int, Future<Unit>>

    init {
        futureMap = HashMap()
    }

    fun cancelRequestForId(id: Int) =
            futureMap[id]?.cancel(true)

    abstract fun requestForImageId(listByTagsResponse: ListByTagsResponse, successCallback: (File) -> Unit, failedCallback: ((Exception) -> Unit)? = null)
}

@Deprecated("")
object TNImageProvider : ImageProvider(){

    override fun requestForImageId(listByTagsResponse: ListByTagsResponse, successCallback: (File) -> Unit, failedCallback: ((Exception) -> Unit)?) {

        val tokens = listByTagsResponse.file_url.split("/")
        val fileName = tokens[tokens.size-1]

        val future = doAsync {
            // Try the database first
            val databaseTNName = DAO.getTNFileForId(listByTagsResponse.id)
            if (databaseTNName != null) {
                // See if the file actually exist on filesystem and readable
                val file = YEAB.pathRelativeToInternalCache(fileName)
                if(file.exists() && file.canRead()) {
                    Log.d(CLASS_NAME, "Existing file in local file system for ${listByTagsResponse.id} @ ${file.absolutePath}")
                    successCallback(file)
                    futureMap.remove(listByTagsResponse.id)
                    return@doAsync
                } else {
                    DAO.updateTNFileForId(listByTagsResponse.id, null)
                }
            }

            // Now try the remote
            while (true) {
                val (_, _, result) = E621API.makeGetImageFromURL(listByTagsResponse.preview_url).destination { response, url ->
                    YEAB.pathRelativeToInternalCache(fileName)
                }.response()

                when (result) {
                    is Result.Success<*,*> -> {
                        DAO.updateTNFileForId(listByTagsResponse.id, fileName)
                        successCallback(YEAB.pathRelativeToInternalCache(fileName))
                        futureMap.remove(listByTagsResponse.id)
                        return@doAsync
                    }
                    is Result.Failure<*,*> -> {
                        failedCallback?.invoke(result.error)
                        return@doAsync
                    }
                }
            }
        }
        futureMap[listByTagsResponse.id] = future
    }
}

@Deprecated("")
object FullImageProvider : ImageProvider() {
    override fun requestForImageId(listByTagsResponse: ListByTagsResponse, successCallback: (File) -> Unit, failedCallback: ((Exception) -> Unit)?) {

        val tokens = listByTagsResponse.file_url.split("/")
        val fileName = tokens[tokens.size-1]

        val future = doAsync {
            // Try the database first
            val databaseFullpath = DAO.getFullPathForId(listByTagsResponse.id)
            if (databaseFullpath != null) {
                // See if the file actually exist on filesystem and readable
                val file = YEAB.pathRelativeToInternalCacheFull(fileName)
                if(file.exists() && file.canRead()) {
                    Log.d(CLASS_NAME, "Existing file in local file system for ${listByTagsResponse.id} @ ${file.absolutePath}")
                    successCallback(file)
                    futureMap.remove(listByTagsResponse.id)
                    return@doAsync
                } else {
                    DAO.updateFullPathFileForId(listByTagsResponse.id, null)
                }
            }

            // Now try the remote
            val (_, _, result) = E621API.makeGetImageFromURL(listByTagsResponse.file_url).destination { response, url ->
                YEAB.pathRelativeToInternalCacheFull(fileName)
            }.response()
            when (result) {
                is Result.Success<*,*> -> {
                    DAO.updateFullPathFileForId(listByTagsResponse.id, fileName)
                    successCallback(YEAB.pathRelativeToInternalCacheFull(fileName))
                    futureMap.remove(listByTagsResponse.id)
                    return@doAsync
                }
                is Result.Failure<*,*> -> {
                    failedCallback?.invoke(result.error)
                    futureMap.remove(listByTagsResponse.id)
                    return@doAsync
                }
            }
        }
        futureMap[listByTagsResponse.id] = future
    }

}


