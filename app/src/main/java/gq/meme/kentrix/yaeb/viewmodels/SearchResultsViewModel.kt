package gq.meme.kentrix.yaeb.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.result.Result
import gq.meme.kentrix.yaeb.dto.ListByTagsResponse
import gq.meme.kentrix.yaeb.utils.CLASS_NAME
import gq.meme.kentrix.yaeb.utils.E621API

class SearchResultsViewModel : ViewModel() {

    private val _results: MutableLiveData<MutableList<ListByTagsResponse>> by lazy { MutableLiveData<MutableList<ListByTagsResponse>>() }
    val results: LiveData<MutableList<ListByTagsResponse>>
        get() = _results
    var isLoading: Boolean = false
        private set
    private var tags: String? = null
    private var tagsSet: Boolean = false
    private var requestHolder: Request? = null
    private val wrappedData: MutableList<ListByTagsResponse> by lazy { ArrayList<ListByTagsResponse>() }

    fun loadMore(noMoreCallback: (() -> Unit), onFailureCallback: ((Exception) -> Unit)) {
        if (!tagsSet) throw Exception("Tags not set when calling loadMore()") // implies tags is not null
        if (isLoading) return // ignore request when we are loading
        requestHolder = E621API.makeListQueryByTag(tags!!, wrappedData.lastOrNull()?.id)
        requestHolder!!.responseObject(ListByTagsResponse.Deserializer){ req, resp, result->
            when (result) {
                is Result.Success<*,*> -> {
                    val newData = result.get()
                    if (newData.isEmpty()) {
                        // We dont have more results to display
                        noMoreCallback()
                        return@responseObject
                        }
                    wrappedData.addAll(newData)
                }
                is Result.Failure<*,*> -> {
                    Log.d(CLASS_NAME, "Request for list results failed, reason ${result.error}")
                    onFailureCallback(result.error)
                }
            }
        }
    }

    fun setQueryTags(newTags: String) {
        if (tagsSet) throw Exception("Tags already set for this view model, you should clear or create another")
        tags = newTags
        tagsSet = true
        _results.value = wrappedData
    }

    override fun onCleared() {
        super.onCleared()
        tags = null
        tagsSet = false
        isLoading = false
        requestHolder?.cancel()
        requestHolder = null
    }

}