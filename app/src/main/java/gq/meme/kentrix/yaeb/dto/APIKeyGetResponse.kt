package gq.meme.kentrix.yaeb.dto

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class APIKeyGetResponse (
        val name: String?,
        val password_hash: String?,
        val success: Boolean?
)
object APIKeyGetResponseDeseialiser: ResponseDeserializable<APIKeyGetResponse> {
    override fun deserialize(content: String): APIKeyGetResponse? {
        return Gson().fromJson(content, APIKeyGetResponse::class.java)
    }
}
