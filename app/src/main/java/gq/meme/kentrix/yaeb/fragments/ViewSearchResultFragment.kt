package gq.meme.kentrix.yaeb.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import gq.meme.kentrix.yaeb.R
import gq.meme.kentrix.yaeb.activities.FragmentEventListener
import android.arch.lifecycle.ViewModelProviders
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.ProgressBar
import android.widget.TextView
import com.github.kittinunf.result.Result
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import es.dmoral.toasty.Toasty
import gq.meme.kentrix.yaeb.activities.ViewImageDetail
import gq.meme.kentrix.yaeb.dto.ListByTagsResponse
import gq.meme.kentrix.yaeb.layouts.ThumbnailViewAdapter
import gq.meme.kentrix.yaeb.utils.CLASS_NAME
import gq.meme.kentrix.yaeb.utils.E621API
import gq.meme.kentrix.yaeb.utils.InfiniteScrollListener
import gq.meme.kentrix.yaeb.utils.bindView
import gq.meme.kentrix.yaeb.viewmodels.*
import kotlinx.android.synthetic.main.fragment_view_search_result.*
import kotlinx.android.synthetic.main.fragment_view_search_result.view.*
import kotlinx.android.synthetic.main.tn_progress_bar.*
import kotlinx.android.synthetic.main.tn_progress_bar.view.*
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.act
import java.util.Observer

class ViewSearchResultFragment : EventfulFragment(), ThumbnailViewAdapter.OnImageClickedListener {

    private var observerHolder: Observer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStop() {
        super.onStop()
        if (observerHolder != null) {
            ObservableStateContainer.deleteObserver(observerHolder)
            observerHolder = null
        }
    }

    override fun onImageClicked(pos: Int) {
        listener?.onFragmentEvent(ViewImageDetail(pos))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_view_search_result, container, false)
        val flexboxManger = FlexboxLayoutManager(context)
        flexboxManger.flexDirection = FlexDirection.ROW
        flexboxManger.justifyContent = JustifyContent.SPACE_AROUND

        view.thumbnailRecycler.layoutManager = flexboxManger

        val adapter: ThumbnailViewAdapter = ThumbnailViewAdapter(this)

        view.thumbnailRecycler.adapter = adapter

        val listener = object : InfiniteScrollListener(layoutManager = flexboxManger) {
            override fun onLoadMore() {
                ObservableStateContainer.loadMore()
            }
        }

        view.thumbnailRecycler.addOnScrollListener(listener)

        observerHolder = Observer { _, arg ->
            when (arg)  {
                is RefreshView -> {
                    Log.d(CLASS_NAME, "Received RefreshView, ${arg.posStart} ${arg.siz}")
                    adapter.notifyItemRangeChanged(arg.posStart, arg.siz)
                    adapter.notifyDataSetChanged()
                }
                is NoMoreData ->  {
                    Toasty.info(activity!!, "No more results!").show()
                    activity!!.runOnUiThread {
                        view.TNProgressBar.visibility = View.GONE
                        Toasty.info(activity!!, "No more results!").show()
                        thumbnailRecycler.removeOnScrollListener(listener)
                    }
                }
                is LoadFailed -> {
                    Log.d(CLASS_NAME, "Load failed: ex ${arg.ex}")
                    adapter.notifyLoadFailed()
                }
                else -> Log.wtf(CLASS_NAME, "Unknown message received")
            }
        }

        ObservableStateContainer.addObserver(observerHolder!!)
        return view
    }


    companion object {
        @JvmStatic
        fun newInstance() = ViewSearchResultFragment()
    }
}
