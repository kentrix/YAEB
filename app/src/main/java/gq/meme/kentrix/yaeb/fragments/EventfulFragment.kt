package gq.meme.kentrix.yaeb.fragments

import android.content.Context
import android.support.v4.app.Fragment
import gq.meme.kentrix.yaeb.activities.FragmentEventListener

/**
 * A special type of Fragment which requires the parent context to be a FragmentEventListener
 * which we can send event to
 * @throws RuntimeException if the attached context is not type of `FragmentEventListener`
 */
abstract class EventfulFragment : Fragment() {

    protected var listener: FragmentEventListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        retainInstance = true
        if (context is FragmentEventListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement FragmentEventListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}


