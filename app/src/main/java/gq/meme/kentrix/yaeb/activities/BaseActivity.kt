package gq.meme.kentrix.yaeb.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import gq.meme.kentrix.yaeb.R
import gq.meme.kentrix.yaeb.dao.DAO
import gq.meme.kentrix.yaeb.dto.UserProfile
import gq.meme.kentrix.yaeb.fragments.*
import gq.meme.kentrix.yaeb.utils.CLASS_NAME
import gq.meme.kentrix.yaeb.utils.bindView
import gq.meme.kentrix.yaeb.viewmodels.ObservableStateContainer
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.app_bar_base.*
import kotlinx.android.synthetic.main.content_base.*

open class BaseActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, FragmentEventListener {
    protected var fullScreened = false

    private val handler = Handler()

    private fun restoreFullscreen() {
        if (supportFragmentManager.findFragmentById(R.id.fragmentContainer) is FullscreenedFragment) {
            fullscreenFun()
        }
    }

    override fun onResume() {
        super.onResume()
        restoreFullscreen()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        if (savedInstanceState == null) {
            val searchFrag = SearchFragment.newInstance()

            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainer, searchFrag)
                commit()
            }
        } else {
            // Android reset top bar UI when rotating screen, so we restore this
            restoreFullscreen()
        }
    }

    private fun fullscreenFun() {
        toolbar.visibility = View.GONE
        handler.post(BaseActivity.hideRunnable(drawer_layout))
        fullScreened = true
    }

    private fun unfullscreenFun() {
        toolbar.visibility = View.VISIBLE
        fullScreened = false
        drawer_layout.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }

    override fun onFragmentEvent(event: MsgToBaseActivity) {
        Log.d(CLASS_NAME, "$event received")
        when(event) {
            is NewSearchWithTags -> {
                supportFragmentManager.beginTransaction().apply {
                    addToBackStack(null)
                    replace(R.id.fragmentContainer, ViewSearchResultFragment.newInstance())
                    ObservableStateContainer.apply {
                        clear()
                        setQueryTags(event.tags)
                    }
                    commit()
                }
            }
            is ViewImageDetail -> {
                supportFragmentManager.beginTransaction().apply {
                    addToBackStack(null)
                    replace(R.id.fragmentContainer, SwipeViewFragment.newInstance(event.pos))
                    commit()
                }
                fullscreenFun()
            }
            is ToggleOverlayRequest -> {
                (supportFragmentManager.findFragmentById(R.id.fragmentContainer) as? SwipeViewFragment)?.toggleOverlay()
            }
            is LogonSuccessful -> {
                supportFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainer, UserProfileFragment.newInstance())
                    commit()
                }
            }
            is LogoutSuccesful -> {
                supportFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainer, LoginFragment.newInstance())
                    commit()
                }

            }
        }
    }

    override fun onBackPressed() {
        when {
            drawer_layout.isDrawerOpen(GravityCompat.START) -> drawer_layout.closeDrawer(GravityCompat.START)
            fullScreened -> { unfullscreenFun(); super.onBackPressed() }
            else -> super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.base, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        Log.d(CLASS_NAME, "Navigation item clicked ${item}")
        when (item.itemId) {
            R.id.nav_search -> {
                supportFragmentManager.beginTransaction().apply{
                    replace(R.id.fragmentContainer, SearchFragment.newInstance())
                    commit()
                }
            }
            R.id.nav_settings -> {

            }
            R.id.nav_fav -> {

            }
            R.id.nav_user -> {
                if (DAO.getLoggedOnUser() == null) {
                    supportFragmentManager.beginTransaction().apply{
                        replace(R.id.fragmentContainer, LoginFragment.newInstance())
                        commit()
                    }
                } else {
                    supportFragmentManager.beginTransaction().apply{
                        replace(R.id.fragmentContainer, UserProfileFragment.newInstance())
                        commit()
                    }

                }

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    companion object {
        @JvmStatic
        fun hideRunnable(view: View) = Runnable {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            view.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }
}

sealed class MsgToBaseActivity
class NewSearchWithTags(val tags: String) : MsgToBaseActivity()
class ViewImageDetail(val pos: Int) : MsgToBaseActivity()
class FullScreenRequest(val unfullscreen: Boolean = false) : MsgToBaseActivity()
class LogonSuccessful(val userProfile: UserProfile) : MsgToBaseActivity()
object LogoutSuccesful : MsgToBaseActivity()
object ToggleFullScreenRequest : MsgToBaseActivity()
object ToggleOverlayRequest : MsgToBaseActivity()

interface FragmentEventListener {
    fun onFragmentEvent(event: MsgToBaseActivity)
}

interface FullscreenedFragment

