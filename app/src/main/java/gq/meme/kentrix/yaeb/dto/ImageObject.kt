package gq.meme.kentrix.yaeb.dto

data class ImageObject(
        val id : Int,
        val relativePathTN: String?,
        val relativePathFull: String?,
        val favedByUser: List<String>,
        val upvotedByUser: List<String>,
        val downvotedByUser: List<String>
)
