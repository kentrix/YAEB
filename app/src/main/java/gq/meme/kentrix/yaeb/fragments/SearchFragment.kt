package gq.meme.kentrix.yaeb.fragments

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.MultiAutoCompleteTextView

import gq.meme.kentrix.yaeb.R
import gq.meme.kentrix.yaeb.activities.NewSearchWithTags
import kotlinx.android.synthetic.main.fragment_search.view.*
import mehdi.sakout.fancybuttons.FancyButton

class SearchFragment : EventfulFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        view.searchButton.setOnClickListener {
            val tags = view.userTagsInput.text.toString()
            listener?.onFragmentEvent(NewSearchWithTags(tags))
            (context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
        }

        view.userTagsInput.setOnKeyListener { _, _, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyEvent.keyCode == KeyEvent.KEYCODE_ENTER) {
                view.searchButton.callOnClick()
                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }

        view.userTagsInput.setAdapter(ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, listOf("scalie")))

        return view
    }



    companion object {
        @JvmStatic
        fun newInstance() =
                SearchFragment().apply {
                }
    }
}
