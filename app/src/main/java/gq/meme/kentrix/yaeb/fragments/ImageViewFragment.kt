package gq.meme.kentrix.yaeb.fragments

import android.content.Context
import android.os.Bundle
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.backends.pipeline.Fresco
import gq.meme.kentrix.yaeb.R
import gq.meme.kentrix.yaeb.activities.BaseActivity
import gq.meme.kentrix.yaeb.activities.ToggleFullScreenRequest
import gq.meme.kentrix.yaeb.activities.ToggleOverlayRequest
import gq.meme.kentrix.yaeb.common.zoomable.AbstractAnimatedZoomableController
import gq.meme.kentrix.yaeb.common.zoomable.DoubleTapGestureListener
import gq.meme.kentrix.yaeb.common.zoomable.ZoomableController
import gq.meme.kentrix.yaeb.dto.ListByTagsResponse
import kotlinx.android.synthetic.main.fragment_swipe_views.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.sdk25.coroutines.onTouch

class ImageViewFragment: android.support.v4.app.Fragment() {

    var listByTagsResponse: ListByTagsResponse? = null
    var resetImageZoomFn: (() -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_swipe_views, container, false)
        rootView.detailedImageViewProgress.visibility = View.GONE
        rootView.detailedImageView.setTapListener(DoubleTapGestureListener(rootView.detailedImageView, Runnable {
            (activity as? BaseActivity)?.onFragmentEvent(ToggleOverlayRequest)
        }))
        rootView.detailedImageView.setAllowTouchInterceptionWhileZoomed(true)
        val draweeController = Fresco.newDraweeControllerBuilder().setUri(listByTagsResponse!!.file_url).build()

        rootView.detailedImageView.controller = draweeController
        resetImageZoomFn = fun() {
            rootView.detailedImageView.resetZoom()
        }
        view?.requestLayout()

        return rootView
    }

    fun resetZoom() = resetImageZoomFn?.invoke()

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        listByTagsResponse = args?.get(ARG_LIST_BY_TAGS_RESPONSE) as ListByTagsResponse
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_LIST_BY_TAGS_RESPONSE = "list_by_tags_response"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(listByTagsResponse: ListByTagsResponse): ImageViewFragment {
            val fragment = ImageViewFragment()
            val bundle = Bundle()
            bundle.putParcelable(ARG_LIST_BY_TAGS_RESPONSE, listByTagsResponse)
            fragment.arguments = bundle
            return fragment
        }
    }
}
