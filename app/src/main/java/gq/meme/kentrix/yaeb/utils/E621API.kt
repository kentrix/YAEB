package gq.meme.kentrix.yaeb.utils

import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import gq.meme.kentrix.yaeb.dto.UserProfile

/**
 * A Rate limited(TODO) API for E621
 */
object E621API {

    val BASE_PATH = "https://e621.net"
    val POST_PATH = "$BASE_PATH/post"
    val USER_PATH = "$BASE_PATH/user"
    val TAG_PATH  = "$BASE_PATH/tag"
    val FAV_PATH  = "$BASE_PATH/favorite"
    val FAV_CREATE_PATH = "$FAV_PATH/create.json"
    val FAV_DEL_PATH = "$FAV_PATH/destroy.json"
    val API_KEY_PATH = "$BASE_PATH/user/login.json"
    val VOTE_PATH = "$POST_PATH/vote.json"

    init {
        val fuelManager = FuelManager.instance
        fuelManager.baseHeaders = mapOf("User-Agent" to "YAEB-0.0.1 (By ecdubs)")
        fuelManager.timeoutInMillisecond = 3000
        fuelManager.timeoutReadInMillisecond = 3000
    }

    fun makeListQueryByTag(tags: String, beforeId: Int? = null) : Request {
        Log.d(CLASS_NAME, "tag ${tags}, before ${beforeId}")
        val url = "$POST_PATH/index.json"
        return Fuel.get(url, listOf("tags" to tags, "before_id" to beforeId))
    }

    fun makeGetImageFromURL(url: String) : Request {
        Log.d(CLASS_NAME, "Get image $url")
        return Fuel.download(url).timeout(1000)
    }

    fun makeGetPossibleTags() : Request {
        Log.d(CLASS_NAME, "Get All possible tags")
        return Fuel.get(TAG_PATH, listOf("order" to "count"))
    }

    fun makeGetAPIKey(userName: String, password: String) : Request {
        Log.d(CLASS_NAME, "Getting API key")
        return Fuel.get(API_KEY_PATH, listOf("name" to userName, "password" to password))
    }


    /**
     * An authenticated request to fav a post
     */
    fun makeFav(userProfile: UserProfile, postID: String) : Request {
        Log.d(CLASS_NAME, "Faving $postID")
        return Fuel.post(FAV_CREATE_PATH,getAuthParamFromUserProfile(userProfile) + (listOf("id" to postID)) )
    }

    /**
     * An authenticated request to unfav a post
     */
    fun makeUnfav(userProfile: UserProfile, postID: String) : Request {
        Log.d(CLASS_NAME, "Faving $postID")
        return Fuel.post(FAV_DEL_PATH,getAuthParamFromUserProfile(userProfile) + (listOf("id" to postID)) )
    }

    /**
     * Helper function for making vote requests
     * @param userProfile the current logged on user
     * @param postID the post to vote
     * @param voteUp True to vote up, False to vote down
     * @return Fuel.Request
     */
    private fun makeVote(userProfile: UserProfile, postID: String, voteUp: Boolean) : Request {
        Log.d(CLASS_NAME, "Voting $postID, $voteUp")
        return Fuel.post(VOTE_PATH, getAuthParamFromUserProfile(userProfile) + (listOf("id" to postID)) )
    }

    fun makeVoteUp(userProfile: UserProfile, postID: String) = makeVote(userProfile, postID, true)
    fun makeVoteDown(userProfile: UserProfile, postID: String) = makeVote(userProfile, postID, false)


    private fun getAuthParamFromUserProfile(userProfile: UserProfile) : List<Pair<String, String>> {
        return listOf("login" to userProfile.userName, "password_hash" to userProfile.apiKey)
    }

}

