@file:Suppress("UNCHECKED_CAST")

package gq.meme.kentrix.yaeb.viewmodels

import android.arch.lifecycle.LifecycleObserver
import android.util.Log
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.result.Result
import gq.meme.kentrix.yaeb.dao.DAO
import gq.meme.kentrix.yaeb.dto.FavResponse
import gq.meme.kentrix.yaeb.dto.FavResponseDeseialiser
import gq.meme.kentrix.yaeb.dto.ListByTagsResponse
import gq.meme.kentrix.yaeb.dto.UserProfile
import gq.meme.kentrix.yaeb.utils.CLASS_NAME
import gq.meme.kentrix.yaeb.utils.E621API
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean


// A container for all the data we need for the app
// Stores data for one session, persistent data should be managed by DAO
object ObservableStateContainer : Observable(), LifecycleObserver {

    private val _listByTagsResponse: MutableList<ListByTagsResponse>
    val listByTagsResponse: List<ListByTagsResponse>
        get() = _listByTagsResponse
    val isLoading: AtomicBoolean = AtomicBoolean()
    private var tags: String? = null
    private var tagsSet: Boolean = false
    private var requestHolder: Request? = null

    init {
        _listByTagsResponse = ArrayList()
        isLoading.set(false)
    }

    private fun requestFavInternal(postID: String,
                   successCB: (String, FavResponse) -> Any,
                   failureCB: (String, Any) -> Any, isFav: Boolean) {
        val currentUser = DAO.getLoggedOnUser()

        when {
            currentUser == null -> failureCB(postID, UserNotLoggedInError)
            isFav -> E621API.makeFav(currentUser, postID).responseObject(FavResponseDeseialiser) { req, resp, result->
                when (result) {
                    is Result.Success<*,*> -> {
                        val respObj = result.get()
                        if (respObj.success) {
                            // The returned JSON indicated that it was successful
                            DAO.addFavForUser(currentUser.userName, postID)
                            successCB(postID, respObj)
                        } else {
                            // Failure but deserialization is ok, already favoriated
                            failureCB(postID, FavAlreadyExists)
                        }
                    }
                    is Result.Failure<*,*> -> {
                        failureCB(postID, FavCreateRemoteFailure(result))
                    }
                }
            }
            !isFav ->
                E621API.makeUnfav(currentUser, postID).responseObject(FavResponseDeseialiser) { req, resp, result->
                    when (result) {
                        is Result.Success<*,*> -> {
                            val respObj = result.get()
                            if (respObj.success) {
                                // The returned JSON indicated that it was successful
                                DAO.delFavForUser(currentUser!!.userName, postID)
                                successCB(postID, respObj)
                            } else {
                                // Failure but deserialization is ok, already favoriated
                                failureCB(postID, FavNotExists)
                            }
                        }
                        is Result.Failure<*,*> -> {
                            failureCB(postID, FavDeleteRemoteFailure(result))
                        }
                    }
                }
        }
    }

    fun requestFav(postID: String,
                   successCB: (String, FavResponse) -> Any,
                   failureCB: (String, FavCreateRequestError) -> Any) {
        requestFavInternal(postID, successCB, failureCB as (String, Any) -> Any, true )
    }

    fun requestUnfav(postID: String,
                   successCB: (String, FavResponse) -> Any,
                   failureCB: (String, FavDeleteRequestError) -> Any) {
        requestFavInternal(postID, successCB, failureCB as (String, Any) -> Any, false)
    }

    fun loadMore() {
       loadMore(null)
    }

    fun loadMore(callBack: ((Result<List<ListByTagsResponse>, Exception>) -> Unit)?) {
        if (!tagsSet) throw Exception("Tags not set when calling loadMore()") // implies tags is not null
        if (!isLoading.compareAndSet(false, true)) return // ignore request when we are loading
        Log.d(CLASS_NAME, "Calling query for search tags ${_listByTagsResponse.lastOrNull()?.id}")
        requestHolder = E621API.makeListQueryByTag(tags!!, _listByTagsResponse.lastOrNull()?.id)
        requestHolder?.responseObject(ListByTagsResponse.Deserializer){ req, resp, result->
            when (result) {
                is Result.Success<*,*> -> {
                    val newData = result.get()
                    if (newData.isEmpty()) {
                        // We dont have more results to display
                        notifyObservers(NoMoreData)
                        isLoading.set(false)
                        return@responseObject
                    }
                    val posStart = _listByTagsResponse.size
                    _listByTagsResponse.addAll(newData)
                    notifyObservers(RefreshView(posStart, newData.size))
                    callBack?.invoke(Result.of(_listByTagsResponse))
                    isLoading.set(false)
                }
                is Result.Failure<*,*> -> {
                    Log.d(CLASS_NAME, "Request for list results failed, reason ${result.error}")
                    notifyObservers(LoadFailed(result.error))
                    callBack?.invoke(Result.error(result.error))
                    isLoading.set(false)
                }
            }
        }
    }

    fun setQueryTags(newTags: String) {
        if (tagsSet) throw Exception("Tags already set for this view model, you should clear or create another")
        tags = newTags
        tagsSet = true
    }

    fun clear() {
        tagsSet = false
        tags = null
        requestHolder?.cancel()
        requestHolder = null
        _listByTagsResponse.clear()
        notifyObservers(RefreshView(0, _listByTagsResponse.size - 1))
    }

    override fun notifyObservers(arg: Any?) {
        Log.d(CLASS_NAME, "Notifying obs: ${countObservers()} , args: $arg")
        setChanged()
        super.notifyObservers(arg)
    }


}

sealed class NotifyResults
object NoMoreData : NotifyResults()
class RefreshView(val posStart: Int, val siz: Int) : NotifyResults()
class LoadFailed(val ex: Exception) : NotifyResults()

interface RemoteRequestError {}

interface FavCreateRequestError {}
object FavAlreadyExists: FavCreateRequestError
class FavCreateRemoteFailure(wrapped: Result.Failure<*, *>): FavCreateRequestError, RemoteRequestError

interface FavDeleteRequestError {}
object FavNotExists: FavDeleteRequestError
class FavDeleteRemoteFailure(wrapper: Result.Failure<*, *>): FavDeleteRequestError, RemoteRequestError

object UserNotLoggedInError: FavCreateRequestError, FavDeleteRequestError
