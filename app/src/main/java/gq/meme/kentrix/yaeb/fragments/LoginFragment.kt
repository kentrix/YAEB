package gq.meme.kentrix.yaeb.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.kittinunf.result.Result
import es.dmoral.toasty.Toasty

import gq.meme.kentrix.yaeb.R
import gq.meme.kentrix.yaeb.activities.LogonSuccessful
import gq.meme.kentrix.yaeb.dao.DAO
import gq.meme.kentrix.yaeb.dto.APIKeyGetResponseDeseialiser
import gq.meme.kentrix.yaeb.dto.UserProfile
import gq.meme.kentrix.yaeb.utils.E621API
import gq.meme.kentrix.yaeb.utils.newUserProfile
import gq.meme.kentrix.yaeb.viewmodels.ObservableStateContainer
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.coroutines.experimental.defer
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.UI

class LoginFragment : EventfulFragment() {
    @SuppressLint("CheckResult")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        view.loginButton.onClick {
            view.loginButton.isEnabled = false
            if (view.loginUserName.text.isNullOrEmpty() || view.loginPassword.text.isNullOrEmpty()) {
                UI {
                    Toasty.error(context!!, "Username and password can't be empty").show()
                }
                view.loginButton.isEnabled = true
                return@onClick
            }
            E621API.makeGetAPIKey(view.loginUserName.text.toString(), view.loginPassword.text.toString()).responseObject(APIKeyGetResponseDeseialiser) { _, _, resp->
                when (resp) {
                    is Result.Success<*,*> -> {
                        val resp = resp.get()
                        if (resp.password_hash == null || resp.name == null) {
                            UI {
                                Toasty.error(context!!, "Login failed").show()
                            }
                            view.loginButton.isEnabled = true
                        } else {
                            val user = newUserProfile(resp.name, resp.password_hash)
                            DAO.setLoggedOnUser(user)
                            listener?.onFragmentEvent(LogonSuccessful(user))
                            view.loginButton.isEnabled = true
                        }
                    }
                    else -> {
                        UI {
                            Toasty.error(context!!, "Login failed").show()
                        }
                        view.loginButton.isEnabled = true
                    }
                }

            }
        }
        return view
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                LoginFragment().apply {
                }
    }
}
