package gq.meme.kentrix.yaeb.utils

import android.content.Context
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.varunest.sparkbutton.SparkEventListener
import es.dmoral.toasty.Toasty
import gq.meme.kentrix.yaeb.dto.UserProfile
import org.jetbrains.anko.imageBitmap
import java.io.File

@JvmOverloads
fun AppCompatActivity.displayOKAlertBox(context: Context, msg: String = "", title: String = "", buttonCallBack: DialogInterface.OnClickListener? = null) {
    val dialog = AlertDialog.Builder(context)
    dialog.setMessage(msg)
    dialog.setTitle(title)
    dialog.setPositiveButton("OK", buttonCallBack)
    dialog.show()
}

val Any.CLASS_NAME : String
    get() = this.javaClass.name


fun newUserProfile(name: String, apiKey: String) : UserProfile {
    return UserProfile(name, apiKey, favIds = HashSet(), blackList = HashSet(), downvotedIds = HashSet(), upvotedIds = HashSet())
}

/**
 * Simple @see{SparkEventListener} with default impl
 */
abstract class SimpleSparkEventListener : SparkEventListener {
    override fun onEventAnimationEnd(button: ImageView?, buttonState: Boolean) {
    }

    override fun onEventAnimationStart(button: ImageView?, buttonState: Boolean) {
    }
}

fun showRequireUserLoginToast(context: Context) {
   Toasty.info(context, "You need to login before you can do that").show()
}

fun showActionFailedToast(context: Context) {
    Toasty.error(context, "Action failed").show()
}

