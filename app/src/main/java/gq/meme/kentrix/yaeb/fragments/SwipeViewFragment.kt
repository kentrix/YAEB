package gq.meme.kentrix.yaeb.fragments

import android.media.Image
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.view.ViewPager.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.view.animation.*
import android.widget.ImageView
import com.github.kittinunf.result.Result

import gq.meme.kentrix.yaeb.R
import gq.meme.kentrix.yaeb.activities.BaseActivity
import gq.meme.kentrix.yaeb.activities.FullScreenRequest
import gq.meme.kentrix.yaeb.activities.FullscreenedFragment
import gq.meme.kentrix.yaeb.dto.ListByTagsResponse
import gq.meme.kentrix.yaeb.utils.CLASS_NAME
import gq.meme.kentrix.yaeb.viewmodels.ObservableStateContainer
import kotlinx.android.synthetic.main.app_bar_base.*
import kotlinx.android.synthetic.main.fragment_swipe_view.*
import kotlinx.android.synthetic.main.fragment_swipe_view.view.*
import java.util.*


class SwipeViewFragment : EventfulFragment(), FullscreenedFragment {
    fun vis() {
        Log.d(CLASS_NAME, "Making overlay visible")
        overlayElements?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in))
        overlayElements?.visibility = View.VISIBLE
    }

    fun invis() {
        Log.d(CLASS_NAME, "Making overlay invisible")
        overlayElements?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_out))
        overlayElements?.visibility = View.INVISIBLE
    }

    fun toggleOverlay() {
        if (overlayVisible) {
            invis()
        } else {
            vis()
        }
        overlayVisible = !overlayVisible
    }

    private var pos: Int? = null
    private val dataList: List<ListByTagsResponse>
        get() = ObservableStateContainer.listByTagsResponse
    private var overlayElements: View? = null
    private var overlayVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            pos = it.getInt(ARG_POS)
        }
    }

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        args?.let {
            pos = it.getInt(ARG_POS)
        }
    }

    override fun onStart() {
        (activity as BaseActivity).onFragmentEvent(FullScreenRequest())
        overlayElements?.visibility = View.INVISIBLE
        super.onStart()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_swipe_view, container, false)
        val adapter = SectionsPagerAdapter(fragmentManager!!)
        overlayElements = rootView.overlayElementWrapper

        rootView.shareViewArea.setOnClickListener { }

        rootView.favViewArea.setOnClickListener {  }

        rootView.voteViewArea.setOnClickListener {  }

        // Set up the ViewPager with the sections adapter.
        rootView.pagerContainer.adapter = adapter
        rootView.pagerContainer.currentItem = pos!!
        rootView.pagerContainer.offscreenPageLimit = 7
        rootView.pagerContainer.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
                if (state == SCROLL_STATE_IDLE) {
                    if (pos!! > 0) {
                        adapter.fragmentMap[pos!!-1]?.resetZoom()
                    }
                    if (pos!! < adapter.fragmentMap.size - 1) {
                        adapter.fragmentMap[pos!!+1]?.resetZoom();
                    }
                }
            }

            override fun onPageSelected(position: Int) {
                pos = position
                if (position > adapter.count - 5 ) {
                    ObservableStateContainer.loadMore {
                        when (it) {
                            is Result.Success<*,*> -> {
                                adapter.notifyDataSetChanged()
                            }
                        }
                    }
                }
            }
        })
        return rootView
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        var currentItem: ImageViewFragment? = null
            private set
        val fragmentMap: MutableMap<Int, ImageViewFragment> = HashMap()

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Log.d(CLASS_NAME, "Showing detail for image ${dataList[position]}")
            val instance = ImageViewFragment.newInstance(dataList[position])
            fragmentMap[position] = instance
            return instance
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            super.destroyItem(container, position, `object`)
            fragmentMap.remove(position)
        }

        override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
            super.setPrimaryItem(container, position, `object`)
            currentItem = `object` as ImageViewFragment
        }

        override fun getCount(): Int {
            return dataList.size
        }


    }

    companion object {
        const val ARG_POS = "pos"
        @JvmStatic
        fun newInstance(pos: Int) =
                SwipeViewFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_POS, pos)
                    }
                }
    }

}
